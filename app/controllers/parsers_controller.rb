class ParsersController < ApplicationController
  skip_before_filter  :verify_authenticity_token
  SERVER_URL = 'http://pushkin-contest.ror.by/quiz'

  def registration
    Token.create(token: params[:token])
    render json: {answer: 'снежные'}
  end

  def quiz
   uri = URI(SERVER_URL)
   parameters = {
      "answer"=> Parser.answer(params),
      "token" => Token.last.token,
      "task_id" => params[:id]
   }  
   Net::HTTP.post_form(uri,parameters)
   render nothing: true
  end
end
