ThinkingSphinx::Index.define :poem, :with => :active_record do
  indexes text 
  has created_at
end
