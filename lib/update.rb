module Update
   URL = 'http://www.ilibrary.ru/text/'
   def update_db
      Poem.destroy_all
      mechanize = Mechanize.new { |agent|
          agent.user_agent_alias = 'Mac Safari'
      }
      page = mechanize.get('http://www.ilibrary.ru/author/pushkin/l.all/index.html')
      links = page.parser.css('.list a')
      links_id = links.map {|l| l.attributes['href'].value.scan(/\d{3}/).join}.drop(15)

      links_id.each do |li|
        page_URL = URL + li + '/p.1/index.html'
        page = mechanize.get(page_URL)
        title = page.parser.css('.title h1').text
        text = page.parser.css('.par').text
        text.gsub!(/\u0097/, "\u2014")
        Poem.create(title: title, text: text)
      end
    end

    def update_strings
      Poem.destroy_all
      mechanize = Mechanize.new { |agent|
          agent.user_agent_alias = 'Mac Safari'
      }
      page = mechanize.get('http://www.ilibrary.ru/author/pushkin/l.all/index.html')
      links = page.parser.css('.list a')
      links_id = links.map {|l| l.attributes['href'].value.scan(/\d{3}/).join}.drop(15)

      links_id.each do |li|
        page_URL = URL + li + '/p.1/index.html'
        page = mechanize.get(page_URL)
        text = page.parser.css('.par').text
        text.gsub!(/\u0097/, "\u2014")
        text.split("\n").each do |str|
        Stroka.create(stroka: str)
        end
      end
    end
end
