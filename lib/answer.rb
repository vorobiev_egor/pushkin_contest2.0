module Answer
  def answer(params)
    @level = params["level"]
    @question = params["question"]
    self.send("answer#{@level}")
  end

  protected 
    def answer1
     Parser.search.title 
    end

    def answer2
      @arr = @question.split("%WORD%")
      @text = Parser.search.text
      @text[@text.index(@arr[0])+@arr[0].length...@text.index(@arr[1])]
    end

    def answer3
      answer2+","+@text[@text.index(@arr[1])+@arr[1].length...@text.index(@arr[2])]
    end

    def answer4
      answer3+","+@text[@text.index(@arr[2])+@arr[2].length...@text.index(@arr[3])]
    end

    def answer5
      word = @question.split(" ").last
      @str = @question.split(" ")[0..-2].join(" ")      
      @text = Parser.search.text
      i = @text.index(@str)+@str.length+1
      while @text[i] != "\n"
        @ans << @text[i]
        i+=1
      end
      @ans+","+word 
    end
end
