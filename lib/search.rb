module Search
  def search 
    self.send("level#{@level}")
  end 

  protected
   def level1
    Poem.find_by("poems. text LIKE ?","%"+@question+"%")
   end

   def level2
    Poem.where("poems. text LIKE ? AND poems.text LIKE ?","%"+@arr[0]+"%","%"+@arr[1]+"%").limit(1).first
   end

   def level3
    Poem.where("poems. text LIKE ? AND poems.text LIKE ? AND poems.text LIKE ?","%"+@arr[0]+"%","%"+@arr[1]+"%","%"+@arr[2]+"%").limit(1).first
   end
   def level4
    Poem.where("poems. text LIKE ? AND poems.text LIKE ? AND poems.text LIKE ? AND poems.text LIKE ?","%"+@arr[0]+"%","%"+@arr[1]+"%","%"+@arr[2]+"%","%"+@arr[3]+"%").limit(1).first
   end
   def level5
    Poem.find_by("poems.text LIKE ?",@str)
   end
end
