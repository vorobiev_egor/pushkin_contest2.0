class CreateStrokas < ActiveRecord::Migration
  def change
    create_table :strokas do |t|
      t.string :stroka

      t.timestamps
    end
  end
end
